package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

    User getUser();

    String getUserId();

    void checkRoles(Role[] roles);

    boolean isAuth();

}