package ru.tsc.babeshko.tm.exception.entity.user;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class UserLockedException extends AbstractException {

    public UserLockedException() {
        super("Error! User is locked...");
    }

}