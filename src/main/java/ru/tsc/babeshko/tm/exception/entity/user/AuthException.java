package ru.tsc.babeshko.tm.exception.entity.user;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class AuthException extends AbstractException {

    public AuthException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}