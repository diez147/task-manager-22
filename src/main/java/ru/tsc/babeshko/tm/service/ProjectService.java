package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.IProjectRepository;
import ru.tsc.babeshko.tm.api.service.IProjectService;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.exception.entity.project.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description)
                .orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, String name, String description, Date dateBegin, Date dateEnd) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        final Project project = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id)
                .orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id)
                .orElseThrow(IdEmptyException::new);
        final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}