package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.IRepository;
import ru.tsc.babeshko.tm.api.service.IService;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.exception.entity.ModelNotFoundException;
import ru.tsc.babeshko.tm.exception.field.IdEmptyException;
import ru.tsc.babeshko.tm.exception.field.IndexIncorrectException;
import ru.tsc.babeshko.tm.exception.field.UserIdEmptyException;
import ru.tsc.babeshko.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (!Optional.ofNullable(model)
                .isPresent()) return null;
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (!Optional.ofNullable(sort)
                .isPresent()) findAll();
        return repository.findAll(sort);
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (!Optional.ofNullable(comparator)
                .isPresent()) findAll();
        return repository.findAll(comparator);
    }

    @Override
    public boolean existsById(final String id) {
        if (!Optional.ofNullable(id)
                .isPresent()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        Optional.ofNullable(id)
                .orElseThrow(IdEmptyException::new);
        Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model
                .orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model
                .orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M remove(final M model) {
        if (!Optional.ofNullable(model)
                .isPresent()) return null;
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        Optional.ofNullable(id)
                .orElseThrow(IdEmptyException::new);
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (!Optional.ofNullable(collection)
                .isPresent()) return;
        repository.removeAll(collection);
    }

}