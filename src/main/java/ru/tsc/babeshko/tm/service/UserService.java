package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.IProjectRepository;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.api.repository.IUserRepository;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.entity.user.EmailExistsException;
import ru.tsc.babeshko.tm.exception.entity.user.LoginExistsException;
import ru.tsc.babeshko.tm.exception.entity.user.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(
            final IUserRepository userRepository,
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        super(userRepository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public User findByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        return repository.findByLogin(login);
    }

     @Override
    public User removeByLogin(final String login) {
         Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
         return repository.removeByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        return repository.findByEmail(email);
    }

    @Override
    public User create(final String login, final String password) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = create(login, password);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = create(login, password);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(final String userId, final String password) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        final User user = findOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(password));
        return user;

    }

    @Override
    public User updateUser(final String userId, final String firstName, final String lastName, final String middleName) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        final User user = findOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExist(final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        final User user = findByLogin(login);
        user.setLocked(false);
    }

    @Override
    public User remove(final User model) {
        if (model == null) throw new UserNotFoundException();
        final User user = super.remove(model);
        if (user == null) return null;
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

}