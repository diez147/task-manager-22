package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.IProjectRepository;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.api.service.IProjectTaskService;
import ru.tsc.babeshko.tm.exception.entity.task.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.UserIdEmptyException;
import ru.tsc.babeshko.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (!Optional.ofNullable(projectId)
                .isPresent()) return;
        if (!Optional.ofNullable(taskId)
                .isPresent()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (!Optional.ofNullable(projectId)
                .isPresent()) return;
        if (!Optional.ofNullable(taskId)
                .isPresent()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (!Optional.ofNullable(projectId)
                .isPresent()) return;
        taskRepository.findAllByProjectId(userId, projectId)
                .stream()
                .forEach(task -> taskRepository.removeById(userId, task.getId()));
        projectRepository.removeById(userId, projectId);
    }

}
