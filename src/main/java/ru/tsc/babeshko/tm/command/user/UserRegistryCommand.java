package ru.tsc.babeshko.tm.command.user;

import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY USER]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}