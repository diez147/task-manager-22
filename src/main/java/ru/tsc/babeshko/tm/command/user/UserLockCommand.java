package ru.tsc.babeshko.tm.command.user;

import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand{

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public String getDescription() {
        return "User lock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

}