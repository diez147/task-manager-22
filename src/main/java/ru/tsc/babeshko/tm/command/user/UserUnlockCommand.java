package ru.tsc.babeshko.tm.command.user;

import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand{

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public String getDescription() {
        return "User lock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

}