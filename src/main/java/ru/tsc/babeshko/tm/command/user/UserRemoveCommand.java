package ru.tsc.babeshko.tm.command.user;

import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand{

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "User remove";
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

}