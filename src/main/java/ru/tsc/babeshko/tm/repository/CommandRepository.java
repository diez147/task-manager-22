package ru.tsc.babeshko.tm.repository;

import ru.tsc.babeshko.tm.api.repository.ICommandRepository;
import ru.tsc.babeshko.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (!Optional.ofNullable(command)
                .isPresent()) return;
        final String name = command.getName();
        if (Optional.ofNullable(name)
                .isPresent()) mapByName.put(name, command);
        final String argument = command.getArgument();
        if (Optional.ofNullable(argument)
                .isPresent()) mapByArgument.put(name, command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (!Optional.ofNullable(name)
                .isPresent()) return null;
        return mapByName.get(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (!Optional.ofNullable(argument)
                .isPresent()) return null;
        return mapByArgument.get(argument);
    }

}