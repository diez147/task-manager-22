package ru.tsc.babeshko.tm.repository;

import ru.tsc.babeshko.tm.api.repository.IRepository;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (!Optional.ofNullable(sort)
                .isPresent()) return findAll();
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M removeByIndex(final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public long getSize() {
        return models.size();
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.remove(collection);
    }

}